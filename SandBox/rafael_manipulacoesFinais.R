


## Base DadosCBBE ####

DadosCBBE <- model$DadosCBBE %>%
  filter(!Marca %in% c("GENERICA", "OPTOUT")) %>% 
  left_join(
    attributosMarca %>%
      filter(Tipo == "Valor"),
    by = c("Respondente", "Marca")
  ) %>%
  mutate(
    CBBE_GERAL_BIN = floor(CBBE_GERAL / 0.75) * 0.75,
    MediaCBBE_GERAL = mean(CBBE_GERAL),
    DesvioMediaCBBE_GERAL = CBBE_GERAL - MediaCBBE_GERAL,
    AW_AWARENESS_Cod = case_when(
      AW_AWARENESS == 1 ~ "Não conhece",
      AW_AWARENESS == 2 ~ "Conhece",
      AW_AWARENESS == 3 ~ "Recall",
      .default = "Top of mind"
    ),
    RL_RELEVANCIA_Cod = case_when(
      RL_RELEVANCIA == 1 ~ "Rejeita",
      RL_RELEVANCIA == 2 ~ "Neutro",
      RL_RELEVANCIA == 3 ~ "Consideraria",
      .default = "Preferida"
    ),
    CC_ULTIMA_COMPRA_cod = case_when(
      CC_ULTIMASCOMPRAS_1.x == 1 ~ "Sim",
      .default = "Não"
    ),
    CC_PenULTIMA_COMPRA_cod = case_when(
      CC_ULTIMASCOMPRAS_2.x == 1 ~ "Sim",
      .default = "Não"
    ),
    CC_AntepenULTIMA_COMPRA_cod = case_when(
      CC_ULTIMASCOMPRAS_3.x == 1 ~ "Sim",
      .default = "Não"
    ),
    Total_CC_ULTIMASCOMPRAS = CC_ULTIMASCOMPRAS_1.x + CC_ULTIMASCOMPRAS_2.x + CC_ULTIMASCOMPRAS_3.x,
    Total_CC_ULTIMASCOMPRAS_Cod = case_when(
      Total_CC_ULTIMASCOMPRAS == 0 ~ "Sem compra",
      Total_CC_ULTIMASCOMPRAS == 1 ~ "Uma compra",
      Total_CC_ULTIMASCOMPRAS == 2 ~ "Duas compras",
      .default = "Três compras"
    ),
    MarketShare = Total_CC_ULTIMASCOMPRAS/3
  ) %>% 
  group_by(Marca) %>%
  mutate(
    SegmentosCBBE_GERAL = case_when(
      Mean_asc >= quantile(Mean_asc, 0.80) ~ "Melhores",
      Mean_asc <= quantile(Mean_asc, 0.20) ~ "Piores",
      TRUE ~ "Medianos"
    )
  ) %>% 
  ungroup()


# Gera base csv
write.table(x = DadosCBBE,
            file = "Resultados/DadosCBBE_Chocolate_Rafael.csv",
            sep = ";",
            dec = ",",
            row.names = FALSE,
            col.names = TRUE,
            quote = FALSE
) 



## Base DadosFunil ####

DadosFunil <- model$Database %>% 
  select(
    Respondente,
    starts_with("AW_TOPMIND_"),
    starts_with("AW_RECALL_"),
    starts_with("AW_CONHECE_"),
    starts_with("RL_PREFERIDA_"),
    starts_with("RL_CONSIDERA_"),
    starts_with("RL_REJEITA_"),
    -ends_with("OUTRO"), -ends_with("DESC"), -ends_with("ESP"), -ends_with("GENERICA"), -ends_with("COD"), -ends_with("NAO_INFORMADO")
  ) %>% 
  distinct(Respondente, .keep_all = TRUE) %>% 
  pivot_longer(
    cols = -Respondente, # Todas as colunas exceto 'Respondente'
    names_to = c("AW", "Tipo", "Marca"), # 'AW' será descartado, 'Tipo' será a nova coluna para 'TOPMIND' e 'RECALL', 'Marca' será a nova coluna para as marcas
    names_sep = "_"
  ) %>% 
  select(-AW) %>%  # Removendo a coluna AW criada temporariamente
  pivot_wider(
    names_from = Tipo, # Criar colunas a partir de 'Tipo'
    values_from = value # Usar valores da coluna resultante
    # names_prefix = "Funil_" # Prefixo para as novas colunas
  ) %>% 
  left_join(
    DadosCBBE %>% 
      select(Respondente, Marca, CBBE_GERAL, CC_ULTIMASCOMPRAS_1.x, CC_ULTIMASCOMPRAS_2.x, CC_ULTIMASCOMPRAS_3.x),
    by = c("Respondente", "Marca")
  ) %>% 
  mutate(
    PerfectScore = 1,
    NAO_CONHECE = ifelse(CONHECE == 1, 0, 1),
    NEUTRO = case_when(
      PREFERIDA == 1 ~ 0,
      CONSIDERA == 1 ~ 0,
      REJEITA == 1 ~ 0,
      .default = 1
    ),
    Total_CC_ULTIMASCOMPRAS = CC_ULTIMASCOMPRAS_1.x + CC_ULTIMASCOMPRAS_2.x + CC_ULTIMASCOMPRAS_3.x,
    MarketShare = Total_CC_ULTIMASCOMPRAS/3
  ) %>%
  fastDummies::dummy_cols(select_columns = "Total_CC_ULTIMASCOMPRAS", remove_first_dummy = FALSE)



# Gera base csv
write.table(x = DadosFunil,
            file = "Resultados/DadosFunil_Chocolate_Rafael.csv",
            sep = ";",
            dec = ",",
            row.names = FALSE,
            col.names = TRUE,
            quote = FALSE
) 




## Base DadosKamakura ####
df_precos <- data.frame(Marca = c("ARCOR", "GAROTO", "HERSHEYS", "LACTA", "NESTLE", "NEUGEBAUER"),
                        PrecoMedio = c(5.87, 6.58, 6.78, 6.76, 6.93, 5.30))

DadosKamakura <- DadosCBBE %>% 
  select(Respondente, Marca, MarketShare, CBBE_GERAL) %>% 
  group_by(Marca) %>% 
  summarise(
    MarketShare = mean(MarketShare),
    CBBE_GERAL = mean(CBBE_GERAL)
  ) %>% 
  ungroup() %>% 
  left_join(
    df_precos, 
    by = c("Marca" = "Marca")
  )
  
# Gera base csv
write.table(x = DadosKamakura,
            file = "Resultados/DadosKamakura_Chocolate_Rafael.csv",
            sep = ";",
            dec = ",",
            row.names = FALSE,
            col.names = TRUE,
            quote = FALSE
)






