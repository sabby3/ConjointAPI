
# Lista de combinações de TipoAgregacao e Marca
tipo_agregacao <- c("1_AO_LEITE", "2_MEIO_AMARGO", "3_AMARGO", "GERAL")
# marcas <- c("ARCOR", "GAROTO", "HERSHEYS", "LACTA", "NESTLE", "NEUGEBAUER")

# Cria um dataframe vazio para armazenar os resultados
resultados_df <- data.frame()

# Loop sobre todas as combinações de TipoAgregacao e Marca
for (ta in tipo_agregacao) {
  # for (marca in marcas) {
    # Filtra os dados para a combinação atual
    dados_filtrados <- DadosCBBE %>% 
      filter(TipoAgregacao == ta)
    
    # Executa a regressão linear múltipla para a combinação atual
    resultado_lm <- lm(
      CBBE_Valor ~ 
        TOPMIND + RECALL + CONHECE + 
        PREFERIDA + CONSIDERA + REJEITA +
        ULTIMASCOMPRASord,
      data = dados_filtrados 
    )
    
    # Calcula a importância relativa
    resultado_importancia <- calc.relimp(object = resultado_lm, type = "first") #, rela = TRUE) 
    
    # Inicializa uma linha de resultado com a combinação atual
    linha_resultado <- data.frame(
      TipoAgregacao = ta,
      # Marca = marca,
      TOPMIND_Imp = resultado_importancia$first["TOPMIND"],
      SoRECALL_Imp = resultado_importancia$first["RECALL"],
      SoCONHECE_Imp = resultado_importancia$first["CONHECE"],
      PREFERIDA_Imp = resultado_importancia$first["PREFERIDA"],
      SoCONSIDERA_Imp = resultado_importancia$first["CONSIDERA"],
      REJEITA_Imp = resultado_importancia$first["REJEITA"],
      ULTIMASCOMPRASord_Imp = resultado_importancia$first["ULTIMASCOMPRASord"],
      # UMACOMPRA_Imp = resultado_importancia$first["UMACOMPRA"],
      # DUASCOMPRAS_Imp = resultado_importancia$first["DUASCOMPRAS"],
      # TRESCOMPRAS_Imp = resultado_importancia$first["TRESCOMPRAS"],
      stringsAsFactors = FALSE
    )
    
    # Adiciona os coeficientes e p-values das regressões simples
    variaveis <- c("TOPMIND", "SoRECALL", "SoCONHECE", 
                   "PREFERIDA", "SoCONSIDERA", "REJEITA",
                   "ULTIMASCOMPRASord")
                   # "UMACOMPRA", "DUASCOMPRAS", "TRESCOMPRAS")

    for (var in variaveis) {
      resultado_lm_simples <- lm(paste("CBBE_Valor ~", var), data = dados_filtrados)
      coeficiente <- coef(summary(resultado_lm_simples))[2, "Estimate"]
      p_value <- coef(summary(resultado_lm_simples))[2, "Pr(>|t|)"]
      linha_resultado[[paste(var, "Coef", sep = "_")]] <- coeficiente
      linha_resultado[[paste(var, "PValue", sep = "_")]] <- p_value
    }
    
    # Adiciona a linha ao dataframe
    resultados_df <- rbind(resultados_df, linha_resultado)
  # }
}

# Converte colunas de percentuais para numéricas
resultados_df[, 3:ncol(resultados_df)] <- lapply(resultados_df[, 3:ncol(resultados_df)], as.numeric)

# Visualiza o dataframe
print(resultados_df)


importancia <- resultados_df %>% 
  pivot_longer(cols = -c(TipoAgregacao)) %>% #, Marca)) %>%
  separate(name, into = c("Nivel2", "Type"), sep = "_") %>% 
  pivot_wider(names_from = Type, values_from = value) %>% 
  # group_by(TipoAgregacao) %>% 
  # mutate(
  #   Imp_COMBINADO = case_when(
  #     Nivel2 == "TOPMIND" ~ sum(Imp[Nivel2 %in% c("CONHECE", "RECALL", "TOPMIND")], na.rm = TRUE),
  #     Nivel2 == "RECALL" ~ sum(Imp[Nivel2 %in% c("CONHECE", "RECALL")], na.rm = TRUE),
  #     Nivel2 == "PREFERIDA" ~ sum(Imp[Nivel2 %in% c("CONSIDERA", "PREFERIDA")], na.rm = TRUE),
  #     Nivel2 == "TRESCOMPRAS" ~ sum(Imp[Nivel2 %in% c("UMACOMPRA", "DUASCOMPRAS", "TRESCOMPRAS")], na.rm = TRUE),
  #     Nivel2 == "DUASCOMPRAS" ~ sum(Imp[Nivel2 %in% c("UMACOMPRA", "DUASCOMPRAS")], na.rm = TRUE),
  #     .default = Imp
  #   )
  # ) %>% 
  # ungroup() %>% 
  mutate(
    Imp = round(Imp, 3),
    # Imp_COMBINADO = round(Imp_COMBINADO, 3),
    Coef = round(Coef, 3),
    PValue = round(PValue, 3),
    Direcao = ifelse(Coef >= 0, "Aumenta", "Diminui"),
    Nivel1 = case_when(
      Nivel2 %in% c("TOPMIND", "SoRECALL", "SoCONHECE") ~ "AWARENESS",
      Nivel2 %in% c("PREFERIDA", "SoCONSIDERA", "REJEITA") ~ "RELEVANCIA", #REJEITA
      .default = "RITUAL"
    )
  ) %>% 
  relocate(Nivel1, .before = Nivel2) %>% 
  # relocate(Imp_COMBINADO, .after = Imp) %>% 
  arrange(TipoAgregacao, Nivel1, Nivel2)



desempenho <- DadosCBBE %>% 
  # rename(
  #   ULTIMASCOMPRAS = CC_ULTIMASCOMPRAS_COMBINADO,
  #   UMACOMPRA = CC_ULTIMASCOMPRAS_COMBINADO_1,
  #   DUASCOMPRAS = CC_ULTIMASCOMPRAS_COMBINADO_2,
  #   TRESCOMPRAS = CC_ULTIMASCOMPRAS_COMBINADO_3
  # ) %>% 
  select(Respondente, TipoAgregacao, Marca, variaveis) %>% 
  group_by(TipoAgregacao, Marca) %>% 
  summarise(across(TOPMIND:ULTIMASCOMPRASord, mean, na.rm = TRUE)) %>% 
  ungroup() %>% 
  pivot_longer(cols = -c(TipoAgregacao, Marca)) %>% 
  rename(
    Nivel2 = name,
    Desempenho = value
    ) %>% 
  mutate(
    Desempenho = round(Desempenho, 3),
    Nivel1 = case_when(
      Nivel2 %in% c("TOPMIND", "RECALL", "CONHECE") ~ "AWARENESS",
      Nivel2 %in% c("PREFERIDA", "CONSIDERA", "NEUTRO") ~ "RELEVANCIA",
      .default = "RITUAL"
    )
    # Nivel2_COD = case_when(
    #   Nivel2 %in% c("TOPMIND", "PREFERIDA", "TRESCOMPRAS") ~ 3,
    #   Nivel2 %in% c("RECALL", "CONSIDERA", "DUASCOMPRAS") ~ 2,
    #   .default = 1
    # )
  )


desempenhoXimportancia <- importancia  %>%
  left_join(
    desempenho, 
    by = c("TipoAgregacao", "Nivel1", "Nivel2")
    ) %>% 
  relocate(Marca, .after = TipoAgregacao) %>% 
  relocate(Nivel1, .after = Nivel1) %>%  
  # relocate(Nivel2_COD, .after = Nivel1) %>% 
  relocate(Desempenho, .before = Imp) %>% 
  arrange(TipoAgregacao, Marca, Nivel1)

  

# Gera base csv
write.table(x = desempenhoXimportancia,
            file = "Resultados/DadosDesempImport_Rafael.csv",
            sep = ";",
            dec = ",",
            row.names = FALSE,
            col.names = TRUE,
            quote = FALSE
)








### Testes

DadosCBBE$CC_ULTIMASCOMPRAS_COMBINADO

DadosCBBE <- DadosCBBE %>% 
  mutate(
    AW_AWARENESS_ROTULADO = relevel(factor(AW_AWARENESS_ROTULADO), ref = "Não conhece"),
    RL_RELEVANCIA_ROTULADO = relevel(factor(RL_RELEVANCIA_ROTULADO), ref = "Neutro"),
    CC_ULTIMASCOMPRAS_COMBINADO_ROTULADO = relevel(factor(CC_ULTIMASCOMPRAS_COMBINADO_ROTULADO), ref = "Sem compra")
  )

DadosCBBE$CC_FREQ

# Geral
lm0 <- lm(
  CBBE_Valor ~ 
    # CC_ULTIMASCOMPRAS_COMBINADO_ROTULADO,
    CC_ULTIMASCOMPRAS_3,
  data = DadosCBBE %>% 
    filter(TipoAgregacao == "1_AO_LEITE" & Marca == "NESTLE")
)
summary(lm0)



# Definir a variável dummy
dummy <- DadosCBBE$Marca

# Resumo estatístico por grupo
DadosCBBE %>%
  filter(TipoAgregacao == "3_AMARGO" & Marca == "LACTA") %>% 
  group_by(PREFERIDA) %>%
  summarise(
    count = n(),
    mean = mean(CBBE_Valor, na.rm = TRUE),
    sd = sd(CBBE_Valor, na.rm = TRUE),
    median = median(CBBE_Valor, na.rm = TRUE),
    IQR = IQR(CBBE_Valor, na.rm = TRUE)
  )

# Teste t de Student para comparar as médias
t.test(CBBE_Valor ~ factor(PREFERIDA), data = DadosCBBE %>% 
         filter(TipoAgregacao == "3_AMARGO" & Marca == "LACTA")
)


# Boxplot para visualizar a distribuição
ggplot(DadosCBBE %>% 
         filter(TipoAgregacao == "GERAL" & Marca == "LACTA"), 
       aes(x = factor(CONSIDERA), y = CBBE_Valor)) +
  geom_boxplot() +
  labs(x = "Var Dummy", y = "CBBE Valor", title = "Boxplot de CBBE Valor por Var Dummy")

# Histogramas segmentados pela variável dummy
ggplot(DadosCBBE, aes(x = CBBE_Valor, fill = factor(PREFERIDA))) +
  geom_histogram(position = "dodge", bins = 30, alpha = 0.7) +
  labs(x = "CBBE Valor", y = "Frequência", fill = "Var Dummy", title = "Histogramas de CBBE Valor por Var Dummy")







