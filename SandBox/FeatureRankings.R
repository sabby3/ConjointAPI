library(FSelector)

dadosModelo <- model$DadosCBBE %>% 
  filter(
    !Marca %in% c(
      "OPTOUT", "GENERICA"
    )
  ) %>% 
  transmute(
    Respondente, Marca, 
    CBBE_GERAL, 
    AW_AWARENESS = factor(AW_AWARENESS), 
    RL_RELEVANCIA = factor(RL_RELEVANCIA), 
    CC_ULTIMASCOMPRAS_1,
    CC_ULTIMASCOMPRAS_2,
    CC_ULTIMASCOMPRAS_3
  ) %>% 
  dummy_cols(
    select_columns = c("AW_AWARENESS", "RL_RELEVANCIA", "Marca"),
    remove_first_dummy = FALSE,
    remove_most_frequent_dummy = FALSE,
    remove_selected_columns = TRUE
  )


# Rankings feature importance
rankings <- list()

#### Ranking 1 - ChiSquared ####
rankings$ChiSquared <- chi.squared(
  formula = CBBE_GERAL ~ .,
  data = dadosModelo %>% 
    dplyr::select(-Respondente)
) %>% 
  as.data.frame() %>% 
  rownames_to_column(
    var = "Feature"
  ) %>% 
  as_tibble() %>% 
  arrange(desc(attr_importance))

rankings$ChiSquared

#### Ranking 2 - InformationGain ####
rankings$InformationGain <- information.gain(
  formula = CBBE_GERAL ~ .,
  data = dadosModelo %>% 
    dplyr::select(-Respondente)
) %>% 
  as.data.frame() %>% 
  rownames_to_column(
    var = "Feature"
  ) %>% 
  as_tibble() %>% 
  arrange(desc(attr_importance))

rankings$InformationGain

#### Ranking 3 - LinearCorrelation ####
rankings$LinearCorrelation <- linear.correlation(
  formula = CBBE_GERAL ~ .,
  data = dadosModelo %>% 
    dplyr::select(-Respondente)
) %>% 
  as.data.frame() %>% 
  rownames_to_column(
    var = "Feature"
  ) %>% 
  as_tibble() %>% 
  arrange(desc(attr_importance))

rankings$LinearCorrelation

#### Ranking 5 - Relief ####
rankings$Relief <- relief(
  formula = CBBE_GERAL ~ .,
  data = dadosModelo %>% 
    dplyr::select(-Respondente)
) %>% 
  as.data.frame() %>% 
  rownames_to_column(
    var = "Feature"
  ) %>% 
  as_tibble() %>% 
  arrange(desc(attr_importance))

rankings$Relief

#### Ranking 6 - RandomForest ####
rankings$RandomForest <- random.forest.importance(
  formula = CBBE_GERAL ~ .,
  data = dadosModelo %>% 
    dplyr::select(-Respondente)
) %>% 
  as.data.frame() %>% 
  rownames_to_column(
    var = "Feature"
  ) %>% 
  as_tibble() %>% 
  arrange(desc(attr_importance))

rankings$RandomForest
