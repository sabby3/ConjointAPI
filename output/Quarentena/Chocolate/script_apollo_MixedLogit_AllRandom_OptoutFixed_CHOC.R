####
## Script Apollo para Mixed Teste
#

# Set core controls
apollo_control <<- list(
  modelName       = "MixedMNLDummyCoding_AllRandom_OptoutFixed_CHOC",
  modelDescr      = "Mixed MNL Dummy Coding - Mensuracao e Decomposicao de Brand Equity",
  indivID         = "Respondente",
  panelData       = TRUE,
  nCores          = 3,
  outputDirectory = "output"
)

database <- model$Database


#### Define os parametros do modelo ####
# Vetores de parametros, incluindo os que serao fixados em zero na estimacao

apollo_beta <<- c(
  
  # ASC
  asc_Optout = 0,
  
  mu_asc_Marca_ARCOR      = 0,
  mu_asc_Marca_GAROTO     = 0,
  mu_asc_Marca_HERSHEYS   = 0,
  mu_asc_Marca_LACTA      = 0,
  mu_asc_Marca_NESTLE     = 0,
  mu_asc_Marca_NEUGEBAUER = 0,
  mu_asc_Marca_GENERICA   = 0,

  sigma_asc_Marca_ARCOR      = 0,
  sigma_asc_Marca_GAROTO     = 0,
  sigma_asc_Marca_HERSHEYS   = 0,
  sigma_asc_Marca_LACTA      = 0,
  sigma_asc_Marca_NESTLE     = 0,
  sigma_asc_Marca_NEUGEBAUER = 0,
  sigma_asc_Marca_GENERICA   = 0,
  
  # Modificador
  mu_Modificador_2_MEIO_AMARGO_ARCOR = 0,
  mu_Modificador_2_MEIO_AMARGO_GAROTO = 0,
  mu_Modificador_2_MEIO_AMARGO_HERSHEYS = 0,
  mu_Modificador_2_MEIO_AMARGO_LACTA = 0,
  mu_Modificador_2_MEIO_AMARGO_NESTLE = 0,
  mu_Modificador_2_MEIO_AMARGO_NEUGEBAUER = 0,
  mu_Modificador_2_MEIO_AMARGO_GENERICA = 0,
  
  sigma_Modificador_2_MEIO_AMARGO_ARCOR = 0,
  sigma_Modificador_2_MEIO_AMARGO_GAROTO = 0,
  sigma_Modificador_2_MEIO_AMARGO_HERSHEYS = 0,
  sigma_Modificador_2_MEIO_AMARGO_LACTA = 0,
  sigma_Modificador_2_MEIO_AMARGO_NESTLE = 0,
  sigma_Modificador_2_MEIO_AMARGO_NEUGEBAUER = 0,
  sigma_Modificador_2_MEIO_AMARGO_GENERICA = 0,
  
  mu_Modificador_3_AMARGO_ARCOR = 0,
  mu_Modificador_3_AMARGO_GAROTO = 0,
  mu_Modificador_3_AMARGO_HERSHEYS = 0,
  mu_Modificador_3_AMARGO_LACTA = 0,
  mu_Modificador_3_AMARGO_NESTLE = 0,
  mu_Modificador_3_AMARGO_NEUGEBAUER = 0,
  mu_Modificador_3_AMARGO_GENERICA = 0,
  
  sigma_Modificador_3_AMARGO_ARCOR = 0,
  sigma_Modificador_3_AMARGO_GAROTO = 0,
  sigma_Modificador_3_AMARGO_HERSHEYS = 0,
  sigma_Modificador_3_AMARGO_LACTA = 0,
  sigma_Modificador_3_AMARGO_NESTLE = 0,
  sigma_Modificador_3_AMARGO_NEUGEBAUER = 0,
  sigma_Modificador_3_AMARGO_GENERICA = 0,
  
  # Preco
  mu_log_b_preco = -5,
  sigma_log_b_preco = -0.01,
  
  # Compromisso Futuro (Relevancia)
  asc_shift_Considera =  0.69882,
  asc_shift_CfNeutro  =  0.00000,
  asc_shift_Rejeita   =  -0.66734,
  
  # Awareness
  asc_shift_Recall     = 0.48423,
  asc_shift_Conhece    = 0.45447,
  asc_shift_NaoConhece = 0.00000,
  
  # Experiencia
  asc_shift_Satisfeito   =  0.08906,
  asc_shift_ExNeutro     =  0.00000,
  asc_shift_Insatisfeito = -0.27594,
  
  # Inercia/ Ritual
  asc_shift_Ultima           =  0.37449,
  asc_shift_NaoUltima        =  0.00000,
  asc_shift_Penultima        =  0.25907,
  asc_shift_NaoPenultima     =  0.00000,
  asc_shift_Antepenultima    =  0.24449,
  asc_shift_NaoAntepenultima =  0.00000
)

### Vector with names (in quotes) of parameters to be kept fixed at their starting value in apollo_beta, use apollo_beta_fixed = c() if none
apollo_fixed <<- c("asc_Optout", "asc_shift_CfNeutro", "asc_shift_NaoConhece", "asc_shift_ExNeutro", "asc_shift_NaoUltima", "asc_shift_NaoPenultima", "asc_shift_NaoAntepenultima")

### Read in starting values for at least some parameters from existing model output file
apollo_beta <- apollo_readBeta(apollo_beta, apollo_fixed, "output/MixedMNLDummyCoding_AllRandom_CHOC", overwriteFixed = FALSE)

### DEFINE RANDOM COMPONENTS

### Set parameters for generating draws
apollo_draws = list(
  interDrawsType = "mlhs",
  interNDraws    = 100,
  interUnifDraws = c(),
  interNormDraws = c("draws_preco", 
                     "draws_arcor", "draws_garoto", "draws_hersheys", "draws_lacta", "draws_nestle", "draws_neugebauer", "draws_generica",
                     "draws_asc_arcor", "draws_asc_garoto", "draws_asc_hersheys", "draws_asc_lacta", "draws_asc_nestle", "draws_asc_neugebauer", "draws_asc_generica"),
  intraDrawsType = "mlhs",
  intraNDraws    = 0,
  intraUnifDraws = c(),
  intraNormDraws = c()
)

#### Create random parameters ####
apollo_randCoeff = function(apollo_beta, apollo_inputs){
  randcoeff = list()
  
  randcoeff[["b_preco"]] = -exp( mu_log_b_preco + sigma_log_b_preco * draws_preco )

  randcoeff[["asc_Marca_ARCOR"]]     = ( mu_asc_Marca_ARCOR     + sigma_asc_Marca_ARCOR * draws_asc_arcor )
  randcoeff[["asc_Marca_GAROTO"]]    = ( mu_asc_Marca_GAROTO    + sigma_asc_Marca_GAROTO * draws_asc_garoto )
  randcoeff[["asc_Marca_HERSHEYS"]]  = ( mu_asc_Marca_HERSHEYS  + sigma_asc_Marca_HERSHEYS * draws_asc_hersheys )
  randcoeff[["asc_Marca_LACTA"]]     = ( mu_asc_Marca_LACTA     + sigma_asc_Marca_LACTA * draws_asc_lacta )
  randcoeff[["asc_Marca_NESTLE"]]    = ( mu_asc_Marca_NESTLE     + sigma_asc_Marca_NESTLE * draws_asc_nestle )
  randcoeff[["asc_Marca_NEUGEBAUER"]] = ( mu_asc_Marca_NEUGEBAUER     + sigma_asc_Marca_NEUGEBAUER * draws_asc_neugebauer )
  randcoeff[["asc_Marca_GENERICA"]]     = ( mu_asc_Marca_GENERICA     + sigma_asc_Marca_GENERICA * draws_asc_generica )

  randcoeff[["b_Modificador_2_MEIO_AMARGO_ARCOR"]]     = ( mu_Modificador_2_MEIO_AMARGO_ARCOR     + sigma_Modificador_2_MEIO_AMARGO_ARCOR     * draws_arcor )
  randcoeff[["b_Modificador_2_MEIO_AMARGO_GAROTO"]]    = ( mu_Modificador_2_MEIO_AMARGO_GAROTO    + sigma_Modificador_2_MEIO_AMARGO_GAROTO    * draws_garoto )
  randcoeff[["b_Modificador_2_MEIO_AMARGO_HERSHEYS"]]  = ( mu_Modificador_2_MEIO_AMARGO_HERSHEYS  + sigma_Modificador_2_MEIO_AMARGO_HERSHEYS  * draws_hersheys )
  randcoeff[["b_Modificador_2_MEIO_AMARGO_LACTA"]]     = ( mu_Modificador_2_MEIO_AMARGO_LACTA     + sigma_Modificador_2_MEIO_AMARGO_LACTA     * draws_lacta )
  randcoeff[["b_Modificador_2_MEIO_AMARGO_NESTLE"]]    = ( mu_Modificador_2_MEIO_AMARGO_NESTLE    + sigma_Modificador_2_MEIO_AMARGO_NESTLE    * draws_nestle )
  randcoeff[["b_Modificador_2_MEIO_AMARGO_NEUGEBAUER"]]= ( mu_Modificador_2_MEIO_AMARGO_NEUGEBAUER+ sigma_Modificador_2_MEIO_AMARGO_NEUGEBAUER*  draws_neugebauer )
  randcoeff[["b_Modificador_2_MEIO_AMARGO_GENERICA"]]  = ( mu_Modificador_2_MEIO_AMARGO_GENERICA  + sigma_Modificador_2_MEIO_AMARGO_GENERICA  * draws_generica )
  
  randcoeff[["b_Modificador_3_AMARGO_ARCOR"]]     = ( mu_Modificador_3_AMARGO_ARCOR     + sigma_Modificador_3_AMARGO_ARCOR     * draws_arcor )
  randcoeff[["b_Modificador_3_AMARGO_GAROTO"]]    = ( mu_Modificador_3_AMARGO_GAROTO    + sigma_Modificador_3_AMARGO_GAROTO    * draws_garoto )
  randcoeff[["b_Modificador_3_AMARGO_HERSHEYS"]]  = ( mu_Modificador_3_AMARGO_HERSHEYS  + sigma_Modificador_3_AMARGO_HERSHEYS  * draws_hersheys )
  randcoeff[["b_Modificador_3_AMARGO_LACTA"]]     = ( mu_Modificador_3_AMARGO_LACTA     + sigma_Modificador_3_AMARGO_LACTA     * draws_lacta )
  randcoeff[["b_Modificador_3_AMARGO_NESTLE"]]    = ( mu_Modificador_3_AMARGO_NESTLE    + sigma_Modificador_3_AMARGO_NESTLE    * draws_nestle )
  randcoeff[["b_Modificador_3_AMARGO_NEUGEBAUER"]]= ( mu_Modificador_3_AMARGO_NEUGEBAUER+ sigma_Modificador_3_AMARGO_NEUGEBAUER*  draws_neugebauer )
  randcoeff[["b_Modificador_3_AMARGO_GENERICA"]]  = ( mu_Modificador_3_AMARGO_GENERICA  + sigma_Modificador_3_AMARGO_GENERICA  * draws_generica )
  
  return(randcoeff)
}


#### Agrupa e valida entradas ####
apollo_inputs <<- apollo_validateInputs()

apollo_probabilities <<- function(apollo_beta, apollo_inputs, functionality = "estimate"){
  
  ### Attach inputs and detach after function exit
  apollo_attach(apollo_beta, apollo_inputs)
  on.exit(apollo_detach(apollo_beta, apollo_inputs))
  
  ### Create list of probabilities P
  P = list()
  
  ### Create alternative specific constants and coefficients using interactions with socio-demographics
  asc_Marca_ARCOR_value <- asc_Marca_ARCOR + 
    asc_shift_Considera        * (RL_RELEVANCIA_ARCOR > 2) + 
    # asc_shift_CfNeutro       * (RL_RELEVANCIA_ARCOR == 2) + 
    asc_shift_Rejeita          * (RL_RELEVANCIA_ARCOR == 1) + 
    asc_shift_Recall           * (AW_AWARENESS_ARCOR > 2) + 
    asc_shift_Conhece          * (AW_AWARENESS_ARCOR == 2) + 
    # asc_shift_NaoConhece     * (AW_AWARENESS_ARCOR == 1) + 
    asc_shift_Satisfeito       * (EX_SATISFEITO_ARCOR > 3) + 
    asc_shift_Insatisfeito     * (EX_SATISFEITO_ARCOR %in% c(1,2)) + 
    # asc_shift_ExNeutro         * (EX_SATISFEITO_ARCOR %in% c(0,3)) + 
    asc_shift_Ultima           * (CC_ULTIMASCOMPRAS_1_ARCOR == 1) + 
    # asc_shift_NaoUltima      * (CC_ULTIMASCOMPRAS_1_ARCOR == 0) + 
    asc_shift_Penultima        * (CC_ULTIMASCOMPRAS_2_ARCOR == 1) + 
    # asc_shift_NaoPenultima   * (CC_ULTIMASCOMPRAS_2_ARCOR == 0) + 
    asc_shift_Antepenultima    * (CC_ULTIMASCOMPRAS_3_ARCOR == 1)
  # asc_shift_Antepenultima  * (CC_ULTIMASCOMPRAS_3_ARCOR == 0)
  
  asc_Marca_GAROTO_value <- asc_Marca_GAROTO + 
    asc_shift_Considera        * (RL_RELEVANCIA_GAROTO > 2) + 
    # asc_shift_CfNeutro       * (RL_RELEVANCIA_GAROTO == 2) + 
    asc_shift_Rejeita          * (RL_RELEVANCIA_GAROTO == 1) + 
    asc_shift_Recall           * (AW_AWARENESS_GAROTO > 2) + 
    asc_shift_Conhece          * (AW_AWARENESS_GAROTO == 2) + 
    # asc_shift_NaoConhece     * (AW_AWARENESS_GAROTO == 1) + 
    asc_shift_Satisfeito       * (EX_SATISFEITO_GAROTO > 3) + 
    asc_shift_Insatisfeito     * (EX_SATISFEITO_GAROTO %in% c(1,2)) + 
    # asc_shift_ExNeutro         * (EX_SATISFEITO_GAROTO %in% c(0,3)) + 
    asc_shift_Ultima           * (CC_ULTIMASCOMPRAS_1_GAROTO == 1) + 
    # asc_shift_NaoUltima      * (CC_ULTIMASCOMPRAS_1_GAROTO == 0) + 
    asc_shift_Penultima        * (CC_ULTIMASCOMPRAS_2_GAROTO == 1) + 
    # asc_shift_NaoPenultima   * (CC_ULTIMASCOMPRAS_2_GAROTO == 0) + 
    asc_shift_Antepenultima    * (CC_ULTIMASCOMPRAS_3_GAROTO == 1)
  # asc_shift_Antepenultima  * (CC_ULTIMASCOMPRAS_3_GAROTO == 0)
  
  asc_Marca_HERSHEYS_value <- asc_Marca_HERSHEYS + 
    asc_shift_Considera        * (RL_RELEVANCIA_HERSHEYS > 2) + 
    # asc_shift_CfNeutro       * (RL_RELEVANCIA_HERSHEYS == 2) + 
    asc_shift_Rejeita          * (RL_RELEVANCIA_HERSHEYS == 1) + 
    asc_shift_Recall           * (AW_AWARENESS_HERSHEYS > 2) + 
    asc_shift_Conhece          * (AW_AWARENESS_HERSHEYS == 2) + 
    # asc_shift_NaoConhece     * (AW_AWARENESS_HERSHEYS == 1) + 
    asc_shift_Satisfeito       * (EX_SATISFEITO_HERSHEYS > 3) + 
    asc_shift_Insatisfeito     * (EX_SATISFEITO_HERSHEYS %in% c(1,2)) + 
    # asc_shift_ExNeutro         * (EX_SATISFEITO_HERSHEYS %in% c(0,3)) + 
    asc_shift_Ultima           * (CC_ULTIMASCOMPRAS_1_HERSHEYS == 1) + 
    # asc_shift_NaoUltima      * (CC_ULTIMASCOMPRAS_1_HERSHEYS == 0) + 
    asc_shift_Penultima        * (CC_ULTIMASCOMPRAS_2_HERSHEYS == 1) + 
    # asc_shift_NaoPenultima   * (CC_ULTIMASCOMPRAS_2_HERSHEYS == 0) + 
    asc_shift_Antepenultima    * (CC_ULTIMASCOMPRAS_3_HERSHEYS == 1)
  # asc_shift_Antepenultima  * (CC_ULTIMASCOMPRAS_3_HERSHEYS == 0)
  
  asc_Marca_LACTA_value <- asc_Marca_LACTA + 
    asc_shift_Considera        * (RL_RELEVANCIA_LACTA > 2) + 
    # asc_shift_CfNeutro       * (RL_RELEVANCIA_LACTA == 2) + 
    asc_shift_Rejeita          * (RL_RELEVANCIA_LACTA == 1) + 
    asc_shift_Recall           * (AW_AWARENESS_LACTA > 2) + 
    asc_shift_Conhece          * (AW_AWARENESS_LACTA == 2) + 
    # asc_shift_NaoConhece     * (AW_AWARENESS_LACTA == 1) + 
    asc_shift_Satisfeito       * (EX_SATISFEITO_LACTA > 3) + 
    asc_shift_Insatisfeito     * (EX_SATISFEITO_LACTA %in% c(1,2)) + 
    # asc_shift_ExNeutro         * (EX_SATISFEITO_LACTA %in% c(0,3)) + 
    asc_shift_Ultima           * (CC_ULTIMASCOMPRAS_1_LACTA == 1) + 
    # asc_shift_NaoUltima      * (CC_ULTIMASCOMPRAS_1_LACTA == 0) + 
    asc_shift_Penultima        * (CC_ULTIMASCOMPRAS_2_LACTA == 1) + 
    # asc_shift_NaoPenultima   * (CC_ULTIMASCOMPRAS_2_LACTA == 0) + 
    asc_shift_Antepenultima    * (CC_ULTIMASCOMPRAS_3_LACTA == 1)
  # asc_shift_Antepenultima  * (CC_ULTIMASCOMPRAS_3_LACTA == 0)
  
  asc_Marca_NESTLE_value <- asc_Marca_NESTLE + 
    asc_shift_Considera        * (RL_RELEVANCIA_NESTLE > 2) + 
    # asc_shift_CfNeutro       * (RL_RELEVANCIA_NESTLE == 2) + 
    asc_shift_Rejeita          * (RL_RELEVANCIA_NESTLE == 1) + 
    asc_shift_Recall           * (AW_AWARENESS_NESTLE > 2) + 
    asc_shift_Conhece          * (AW_AWARENESS_NESTLE == 2) + 
    # asc_shift_NaoConhece     * (AW_AWARENESS_NESTLE == 1) + 
    asc_shift_Satisfeito       * (EX_SATISFEITO_NESTLE > 3) + 
    asc_shift_Insatisfeito     * (EX_SATISFEITO_NESTLE %in% c(1,2)) + 
    # asc_shift_ExNeutro         * (EX_SATISFEITO_NESTLE %in% c(0,3)) + 
    asc_shift_Ultima           * (CC_ULTIMASCOMPRAS_1_NESTLE == 1) + 
    # asc_shift_NaoUltima      * (CC_ULTIMASCOMPRAS_1_NESTLE == 0) + 
    asc_shift_Penultima        * (CC_ULTIMASCOMPRAS_2_NESTLE == 1) + 
    # asc_shift_NaoPenultima   * (CC_ULTIMASCOMPRAS_2_NESTLE == 0) + 
    asc_shift_Antepenultima    * (CC_ULTIMASCOMPRAS_3_NESTLE == 1)
  # asc_shift_Antepenultima  * (CC_ULTIMASCOMPRAS_3_NESTLE == 0)
  
  asc_Marca_NEUGEBAUER_value <- asc_Marca_NEUGEBAUER + 
    asc_shift_Considera        * (RL_RELEVANCIA_NEUGEBAUER > 2) + 
    # asc_shift_CfNeutro       * (RL_RELEVANCIA_NEUGEBAUER == 2) + 
    asc_shift_Rejeita          * (RL_RELEVANCIA_NEUGEBAUER == 1) + 
    asc_shift_Recall           * (AW_AWARENESS_NEUGEBAUER > 2) + 
    asc_shift_Conhece          * (AW_AWARENESS_NEUGEBAUER == 2) + 
    # asc_shift_NaoConhece     * (AW_AWARENESS_NEUGEBAUER == 1) + 
    asc_shift_Satisfeito       * (EX_SATISFEITO_NEUGEBAUER > 3) + 
    asc_shift_Insatisfeito     * (EX_SATISFEITO_NEUGEBAUER %in% c(1,2)) + 
    # asc_shift_ExNeutro         * (EX_SATISFEITO_NEUGEBAUER %in% c(0,3)) + 
    asc_shift_Ultima           * (CC_ULTIMASCOMPRAS_1_NEUGEBAUER == 1) + 
    # asc_shift_NaoUltima      * (CC_ULTIMASCOMPRAS_1_NEUGEBAUER == 0) + 
    asc_shift_Penultima        * (CC_ULTIMASCOMPRAS_2_NEUGEBAUER == 1) + 
    # asc_shift_NaoPenultima   * (CC_ULTIMASCOMPRAS_2_NEUGEBAUER == 0) + 
    asc_shift_Antepenultima    * (CC_ULTIMASCOMPRAS_3_NEUGEBAUER == 1)
  # asc_shift_Antepenultima  * (CC_ULTIMASCOMPRAS_3_NEUGEBAUER == 0)
  
  asc_Marca_GENERICA_value <- asc_Marca_GENERICA + 
    asc_shift_Considera        * (RL_RELEVANCIA_GENERICA > 2) + 
    # asc_shift_CfNeutro       * (RL_RELEVANCIA_GENERICA == 2) + 
    asc_shift_Rejeita          * (RL_RELEVANCIA_GENERICA == 1) + 
    asc_shift_Recall           * (AW_AWARENESS_GENERICA > 2) + 
    asc_shift_Conhece          * (AW_AWARENESS_GENERICA == 2) + 
    # asc_shift_NaoConhece     * (AW_AWARENESS_GENERICA == 1) + 
    asc_shift_Satisfeito       * (EX_SATISFEITO_GENERICA > 3) + 
    asc_shift_Insatisfeito     * (EX_SATISFEITO_GENERICA %in% c(1,2)) + 
    # asc_shift_ExNeutro         * (EX_SATISFEITO_GENERICA %in% c(0,3)) + 
    asc_shift_Ultima           * (CC_ULTIMASCOMPRAS_1_GENERICA == 1) + 
    # asc_shift_NaoUltima      * (CC_ULTIMASCOMPRAS_1_GENERICA == 0) + 
    asc_shift_Penultima        * (CC_ULTIMASCOMPRAS_2_GENERICA == 1) + 
    # asc_shift_NaoPenultima   * (CC_ULTIMASCOMPRAS_2_GENERICA == 0) + 
    asc_shift_Antepenultima    * (CC_ULTIMASCOMPRAS_3_GENERICA == 1)
  # asc_shift_Antepenultima  * (CC_ULTIMASCOMPRAS_3_GENERICA == 0)
  
  ### List of utilities: these must use the same names as in mnl_settings, order is irrelevant
  V <- list()
  V[["Marca_ARCOR"]] <- asc_Marca_ARCOR_value + b_preco * Preco_ARCOR + b_Modificador_2_MEIO_AMARGO_ARCOR * (Modificador_ARCOR == 2) + b_Modificador_3_AMARGO_ARCOR * (Modificador_ARCOR == 3)
  V[["Marca_GAROTO"]] <- asc_Marca_GAROTO_value + b_preco * Preco_GAROTO + b_Modificador_2_MEIO_AMARGO_GAROTO * (Modificador_GAROTO == 2) + b_Modificador_3_AMARGO_GAROTO * (Modificador_GAROTO == 3)
  V[["Marca_HERSHEYS"]] <- asc_Marca_HERSHEYS_value + b_preco * Preco_HERSHEYS + b_Modificador_2_MEIO_AMARGO_HERSHEYS * (Modificador_HERSHEYS == 2) + b_Modificador_3_AMARGO_HERSHEYS * (Modificador_HERSHEYS == 3)
  V[["Marca_LACTA"]] <- asc_Marca_LACTA_value + b_preco * Preco_LACTA + b_Modificador_2_MEIO_AMARGO_LACTA * (Modificador_LACTA == 2) + b_Modificador_3_AMARGO_LACTA * (Modificador_LACTA == 3)
  V[["Marca_NESTLE"]] <- asc_Marca_NESTLE_value + b_preco * Preco_NESTLE + b_Modificador_2_MEIO_AMARGO_NESTLE * (Modificador_NESTLE == 2) + b_Modificador_3_AMARGO_NESTLE * (Modificador_NESTLE == 3)
  V[["Marca_NEUGEBAUER"]] <- asc_Marca_NEUGEBAUER_value + b_preco * Preco_NEUGEBAUER + b_Modificador_2_MEIO_AMARGO_NEUGEBAUER * (Modificador_NEUGEBAUER == 2) + b_Modificador_3_AMARGO_NEUGEBAUER * (Modificador_NEUGEBAUER == 3)
  V[["Marca_GENERICA"]] <- asc_Marca_GENERICA_value + b_preco * Preco_GENERICA + b_Modificador_2_MEIO_AMARGO_GENERICA * (Modificador_GENERICA == 2) + b_Modificador_3_AMARGO_GENERICA * (Modificador_GENERICA == 3)
  
  V[["Optout"]] = asc_Optout
  
  ### Define settings for MNL model component
  mnl_settings <- list(
    alternatives = c(
      Marca_ARCOR = 1,
      Marca_GAROTO = 2,
      Marca_HERSHEYS = 3,
      Marca_LACTA = 4,
      Marca_NESTLE = 5,
      Marca_NEUGEBAUER = 6,
      Marca_GENERICA = 7,
      Optout = 9
    ),
    avail = list(
      Marca_ARCOR = Av_ARCOR,
      Marca_GAROTO = Av_GAROTO,
      Marca_HERSHEYS = Av_HERSHEYS,
      Marca_LACTA = Av_LACTA,
      Marca_NESTLE = Av_NESTLE,
      Marca_NEUGEBAUER = Av_NEUGEBAUER,
      Marca_GENERICA = Av_GENERICA,
      Optout = 1
    ),
    choiceVar = ChoiceMarca,
    utilities = V
  )
  
  ### Compute probabilities using MNL model
  P[["model"]] = apollo_mnl(mnl_settings, functionality)
  
  ### Take product across observation for same individual
  P = apollo_panelProd(P, apollo_inputs, functionality)
  
  ### Average across inter-individual draws
  P = apollo_avgInterDraws(P, apollo_inputs, functionality)
  
  ### Prepare and return outputs of function
  P = apollo_prepareProb(P, apollo_inputs, functionality)
  return(P)
}


# ################################################################# #
#### MODEL ESTIMATION                                            ####
# ################################################################# #

# apollo_speedTest(apollo_beta, apollo_fixed, apollo_probabilities, apollo_inputs)

model <<- apollo_estimate(
  apollo_beta = apollo_beta,
  apollo_fixed = apollo_fixed,
  apollo_probabilities = apollo_probabilities,
  apollo_inputs = apollo_inputs
)

apollo_modelOutput(model)
# apollo_saveOutput(model)


# Calculo do CBBE e componentes

un <- apollo_unconditionals(model, apollo_probabilities, apollo_inputs)

un$asc_Marca_HERSHEYS %>% 
  as_tibble() %>% 
  mutate(
    max_all = apply(across(everything(1:50)), 1, max),
    min_all = apply(across(everything(1:50)), 1, min),
    mean_all = apply(across(everything(1:50)), 1, mean),
    sd_all = apply(across(everything(1:50)), 1, sd),
    median_all = apply(across(everything(1:50)), 1, median)
  ) %>% 
  view()


#### Generica ####

# Asc Generica 
# Delta = 
mean(un$asc_Marca_GENERICA/un$b_preco)
(-0.17390)/-0.20866


# Modificador Generica 
# Delta = 
mean(un$b_Modificador_2_MEIO_AMARGO_GENERICA/un$b_preco)
(-0.17390)/-0.20866



#### Neugebauer ####

# Asc Neugebauer 
# Delta = 
mean(un$asc_Marca_NEUGEBAUER/un$b_preco)
(0.31196)/-0.20866

# Modificador meio amargo Neugebauer 
# Delta = 
mean(un$b_Modificador_2_MEIO_AMARGO_NEUGEBAUER/un$b_preco)
(0.05630)/-0.20866

# Fator unico meio amargo Neugebauer 
# Delta = 
mean((un$asc_Marca_NEUGEBAUER + un$b_Modificador_2_MEIO_AMARGO_NEUGEBAUER)/un$b_preco) 
((0.31196 + 0.05630)/-0.20866)


# Asc Neugebauer - Generica
# Delta = 
mean((un$asc_Marca_NEUGEBAUER/un$b_preco) - (un$asc_Marca_GENERICA/un$b_preco))
((0.31196)/-0.20866) - (0/-0.20866)

# meio amargo Neugebauer - Generica
# Delta = 
mean((un$b_Modificador_2_MEIO_AMARGO_NEUGEBAUER/un$b_preco) - (un$b_Modificador_2_MEIO_AMARGO_GENERICA/un$b_preco))
(0.05630/-0.20866) - (-0.17390/-0.20866)

# Fator unico meio amargo Neugebauer - Generica
# Delta = 
mean(((un$asc_Marca_NEUGEBAUER + un$b_Modificador_2_MEIO_AMARGO_NEUGEBAUER)/un$b_preco) - ((un$asc_Marca_GENERICA + un$b_Modificador_2_MEIO_AMARGO_GENERICA)/un$b_preco))
((0.31196 + 0.05630)/-0.20866) - ((0 + -0.17390)/-0.20866)



#### Nestle ####

# Asc Nestle 
# Delta = 
mean(un$asc_Marca_NESTLE/un$b_preco)
(1.14100)/-0.20866

# Modificador meio amargo Nestle 
# Delta = 
mean(un$b_Modificador_2_MEIO_AMARGO_NESTLE/un$b_preco)
(0.24444)/-0.20866

# Fator unico meio amargo Nestle 
# Delta = 
mean((un$asc_Marca_NESTLE + un$b_Modificador_2_MEIO_AMARGO_NESTLE)/un$b_preco) 
((1.14100 + 0.24444)/-0.20866)


# Asc Nestle - Generica
# Delta = -2.433 (-7.901 - -5.468)
mean((un$asc_Marca_NESTLE/un$b_preco) - (un$asc_Marca_GENERICA/un$b_preco))
((1.14100)/-0.20866) - (0/-0.20866)

# meio amargo Nestle - Generica
# Delta = 
mean((un$b_Modificador_2_MEIO_AMARGO_NESTLE/un$b_preco) - (un$b_Modificador_2_MEIO_AMARGO_GENERICA/un$b_preco))
(0.24444/-0.20866) - (-0.17390/-0.20866)

# Fator unico meio amargo Nestle - Generica
# Delta = 
mean(((un$asc_Marca_NESTLE + un$b_Modificador_2_MEIO_AMARGO_NESTLE)/un$b_preco) - ((un$asc_Marca_GENERICA + un$b_Modificador_2_MEIO_AMARGO_GENERICA)/un$b_preco))
((1.14100 + 0.24444)/-0.20866) - ((0 + -0.17390)/-0.20866)



mean((un$b_Modificador_2_MEIO_AMARGO_ARCOR/un$b_preco) - (un$b_Modificador_2_MEIO_AMARGO_GENERICA/un$b_preco))
(0.15413/-0.20866) - (-0.17390/-0.20866)










# Outras coisas

# Calcula o numero de marcas consideradas - proxy para "lealdade"
database$RL_CONSIDERA

database <- database %>%
  mutate(NUM_RESP = sapply(strsplit(RL_CONSIDERA, "\\|"), length))

table(database$NUM_RESP)/7640





###
## Fim
#



